<?php

/**
 * @file
 * Using custom table in views.
 */

/**
 * Implements hook_views_data().
 * @return array
 */
function exrates_views_data() {
  $data = [];
  $data['exrates_store']['table']['group'] = t('Exrates table');
  $data['exrates_store']['table']['base'] = [
    'field' => 'id',
    'title' => 'Exchange rates',
    'help' => 'Exchange rates',
  ];
  $data['exrates_store']['id'] = [
    'title' => t('Id'),
    'help' => t('Id'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];
  $data['exrates_store']['course_date'] = [
    'title' => t('Date'),
    'help' => t('Date'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_date',
    ],
  ];
  $data['exrates_store']['num_code'] = [
    'title' => t('NumCode'),
    'help' => t('NumCode'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];
  $data['exrates_store']['char_code'] = [
    'title' => t('CharCode'),
    'help' => t('CharCode'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];
  $data['exrates_store']['char_code'] = [
    'title' => t('CharCode'),
    'help' => t('CharCode'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];
  $data['exrates_store']['nominal'] = [
    'title' => t('Nominal'),
    'help' => t('Nominal'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];
  $data['exrates_store']['name'] = [
    'title' => t('Name'),
    'help' => t('Name'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];
  $data['exrates_store']['value'] = [
    'title' => t('Value'),
    'help' => t('Value'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_numeric',
    ],
  ];
  return $data;
}
