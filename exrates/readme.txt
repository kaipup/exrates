Module dependencies: views, elysia_cron.
Views adds flexibility and simplicity in displaying data,
Elysia_cron - convenience startup configuration.

After installing the module for storing exchange rates in the database, the "exrates_store" table will be created.

By default, data is requested to the table once a day at 12:00.
Change launch schedule: /admin/config/system/cron/settings
Run the update manually: /admin/config/system/cron/execute/exrates_receiver

The module adds display support through the Views module, so you can easily customize the desired output format. You can also import the view delivered with the module (views_export.txt), then the values ​​will be available in a table form with the ability to sort, filter and paginate: /exchange-rates
