<?php

class Receiver {

  const URL = 'http://www.cbr.ru/scripts/XML_daily.asp';

  // optional value for test: dd/mm/yyyy
  private $date_req = '';

  /**
   * Run from the cron
   */
  public static function run() {
    $receiver = new static();
    $receiver->process();
  }

  /**
   * Process
   */
  private function process() {
    $data = $this->getData();
    if ($data) {
      $this->saveData($data);
    }
  }

  /**
   * Gets data from source
   *
   * @return array
   */
  private function getData() {
    $data = [];

    $date = 0;
    $values = [];

    $url = $this->date_req ? url(self::URL, ['query' => ['date_req' => $this->date_req]]) : self::URL;
    $options = [
      'headers' => [
        'Cache-Control' => 'no-cache',
        'Pragma' => 'no-cache',
      ],
    ];

    $response = drupal_http_request($url, $options);
    if (isset($response->error)) {
      // @todo write to log
      return $data;
    }

    $xml = simplexml_load_string($response->data);
    if (!$xml) {
      // @todo write to log
      return $data;
    }

    $attr = $xml->attributes();
    foreach ($attr as $key => $elem) {
      if ($key == 'Date') {
        $elem = (array)$elem;
        $date = DateTime::createFromFormat('d.m.Y', $elem[0]);
        $date->setTime(0, 0);
        $date = $date->getTimestamp();
      }
    }

    foreach ($xml->Valute as $value) {
      $values[] = (array)$value;
    }

    if ($date && $values) {
      $data = [
        'date' => $date,
        'values' => $values,
      ];
    }

    return $data;
  }

  /**
   * Save data to database
   *
   * @param array $data
   * @throws Exception
   */
  public function saveData(array $data) {
    db_delete('exrates_store')
      ->condition('course_date', $data['date'])
      ->execute();
    foreach ($data['values'] as $v) {
      $row = [
        'course_date' => $data['date'],
        'num_code' => $v['NumCode'],
        'char_code' => $v['CharCode'],
        'nominal' => $v['Nominal'],
        'name' => $v['Name'],
        'value' => str_replace(',', '.', $v['Value']),
      ];
      db_insert('exrates_store')
        ->fields($row)
        ->execute();
    }
  }

}
